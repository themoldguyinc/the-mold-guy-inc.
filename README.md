The Mold Guy was founded in 2005 as the result of a simple observation: dealing with mold contamination is overwhelming and scary. Questions like "Will mold make me sick?" and "Do I need to leave my home right now?" fly through your mind the instant you suspect a mold problem. The truth is that the stress and uncertainty that comes along with an indoor air quality contamination is as big of a problem as the contamination itself.

Website: http://www.themoldguyinc.com/
